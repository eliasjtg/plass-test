import 'package:equatable/equatable.dart';
import 'package:plass_test/model/user.dart';

/// Base Authentication State
class AuthenticationState extends Equatable {

  /// Authentication State Construct
  const AuthenticationState();

  @override
  List<Object> get props => <Object>[];
}

/// Authentication Loading State
class AuthenticationLoading extends AuthenticationState {}

/// Unauthenticated State
class UnAuthenticated extends AuthenticationState {}

/// Authenticated State
class Authenticated extends AuthenticationState {
  /// Authenticated [user]
  final User user;

  /// Authenticated State Constructor
  const Authenticated({required this.user});

  @override
  List<Object> get props => <Object>[user];
}

/// Authentication Failure State
class AuthenticationFailure extends AuthenticationState {

  /// Error [message]
  final String message;

  /// Authentication failure constructor
  const AuthenticationFailure({required this.message});

  @override
  List<Object> get props => <Object>[message];
}
