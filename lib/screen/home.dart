import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:get/get.dart';
import 'package:plass_test/controller/auth.dart';
import 'package:plass_test/state/auth.dart';

/// Home Screen
class HomeScreen extends GetView<AuthenticationController> {

  /// HomeScreen constructor
  const HomeScreen({Key? key}) : super(key: key);

  Authenticated get _authState => controller.state as Authenticated;

  @override
  Widget build(BuildContext context) => Scaffold(
    body: SafeArea(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Card(
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  const CircleAvatar(
                    radius: 65,
                    backgroundImage: AssetImage('assets/avatar.png'),
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(vertical: 10),
                  ),
                  ListTile(
                    title: Text(
                      AppLocalizations.of(context)!.home_full_name_tile,
                    ),
                    subtitle: Text(_authState.user.getFullName()),
                  ),
                  if(_authState.user.auth.email != null) ListTile(
                    title: Text(AppLocalizations.of(context)!.home_email_tile),
                    subtitle: Text(_authState.user.auth.email!),
                  ),
                  ListTile(
                    title: Text(
                      AppLocalizations.of(context)!.home_birthdate_tile,
                    ),
                    subtitle: Text(_authState.user.getStrBirthdate()),
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(vertical: 10),
                  ),
                  ElevatedButton(
                      onPressed: controller.signOut,
                      child: Text(
                          AppLocalizations.of(context)!.button_logout,
                      ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    ),
  );
}
