import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:get/get.dart';
import 'package:plass_test/controller/auth.dart';

/// Splash Screen
class SplashScreen extends GetView<AuthenticationController> {

  /// SplashScreen constructor
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
    body: SafeArea(
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(10),
                child: Image.asset(
                  'assets/logo.png',
                ),
              ),
              const Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
              ),
              Text(
                AppLocalizations.of(context)!.screen_splash_welcome,
                style: Theme.of(context).textTheme.headline5,
                textAlign: TextAlign.center,
              ),
              const Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
              ),
              ElevatedButton(
                onPressed: () => Get.offAndToNamed('/login'),
                child: Text(
                  AppLocalizations.of(context)!.button_continue,
                ),
              ),
            ],
          ),
        ),
      ),
    ),
  );
}
