import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';
import 'package:plass_test/controller/auth.dart';
import 'package:plass_test/screen/partial/or.dart';

/// Login Screen
class LoginScreen extends GetView<AuthenticationController> {

  final Map<String, dynamic> _parameters = <String, dynamic>{};

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  /// LoginScreen constructor
  LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
    body: SafeArea(
      child: Flex(
        direction: Axis.vertical,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(10),
            child: Image.asset(
              'assets/logo.png',
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Form(
                  key: _formKey,
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.all(20),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(AppLocalizations.of(context)!.screen_login_title,
                            style: Theme.of(context).textTheme.headline6,),
                          const Padding(
                            padding: EdgeInsets.symmetric(vertical: 10),
                          ),
                          TextFormField(
                            decoration: InputDecoration(
                              labelText: AppLocalizations.of(context)!.
                              input_email_label,
                              hintText: AppLocalizations.of(context)!.
                              input_email_hint,
                              prefixIcon: const Icon(
                                Icons.email_outlined,
                                size: 20,
                              ),
                            ),
                            keyboardType: TextInputType.emailAddress,
                            validator: MultiValidator(<FieldValidator<String?>>[
                              RequiredValidator(
                                errorText: AppLocalizations.of(context)!
                                    .validation_email_required,
                              ),
                              EmailValidator(
                                errorText: AppLocalizations.of(context)!
                                    .validation_email_invalid,
                              )
                            ]),
                            onSaved: (String? value) {
                              _parameters['email'] = value;
                            },
                          ),
                          const Padding(
                            padding: EdgeInsets.symmetric(vertical: 5),
                          ),
                          TextFormField(
                            obscureText: true,
                            decoration: InputDecoration(
                              labelText: AppLocalizations.of(context)!.
                              input_password_label,
                              hintText: AppLocalizations.of(context)!.
                              input_password_hint,
                              prefixIcon: const Icon(
                                Icons.password,
                                size: 20,
                              ),
                            ),
                            validator: MultiValidator(<FieldValidator<String?>>[
                              RequiredValidator(
                                errorText: AppLocalizations.of(context)!
                                    .validation_password_required,
                              ),
                              MinLengthValidator(
                                8,
                                errorText: AppLocalizations.of(context)!
                                    .validation_password_min,
                              )
                            ]),
                            onSaved: (String? value) {
                              _parameters['password'] = value;
                            },
                          ),
                          const Padding(
                            padding: EdgeInsets.symmetric(vertical: 10),
                          ),
                          ElevatedButton(
                            onPressed: () => _login(context),
                            child: Text(
                              AppLocalizations.of(context)!.button_login,
                            ),
                          ),
                          const Padding(
                            padding: EdgeInsets.symmetric(vertical: 5),
                          ),
                          TextButton(
                            onPressed: () => Get.offAndToNamed('/recover-password'),
                            child: Text(
                              AppLocalizations.of(context)!.
                                button_recover_password,
                            ),
                          ),
                          const Or(vertical: 10,),
                          const Padding(
                            padding: EdgeInsets.symmetric(vertical: 5),
                          ),
                          OutlinedButton(
                            onPressed: () => Get.offAndToNamed('/register'),
                            child: Text(
                              AppLocalizations.of(context)!.button_sign_in,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    ),
  );

  void _login(BuildContext context) {

    if(_formKey.currentState?.validate() ?? false) {
      _formKey.currentState!.save();
      controller.signIn(
        _parameters['email'],
        _parameters['password'],
      ).catchError((Object e, StackTrace trace) {
        if(e is FirebaseAuthException) {
          switch(e.code) {
            case 'invalid-credential':
              Get.snackbar(
                AppLocalizations.of(context)!.screen_login_title,
                AppLocalizations.of(context)!.auth_invalid_credentials,
                backgroundColor: const Color(0xFFFF0033),
                duration: const Duration(seconds: 5),
                colorText: Colors.white,
              );
              break;
            case 'user-disabled':
              Get.snackbar(
                AppLocalizations.of(context)!.screen_login_title,
                AppLocalizations.of(context)!.auth_user_disabled,
                backgroundColor: const Color(0xFFFF0033),
                duration: const Duration(seconds: 5),
                colorText: Colors.white,
              );
              break;
            case 'user-not-found':
              Get.snackbar(
                AppLocalizations.of(context)!.screen_login_title,
                AppLocalizations.of(context)!.auth_user_not_found,
                backgroundColor: const Color(0xFFFF0033),
                duration: const Duration(seconds: 5),
                colorText: Colors.white,
              );
              break;
            case 'wrong-password':
              Get.snackbar(
                AppLocalizations.of(context)!.screen_login_title,
                AppLocalizations.of(context)!.auth_wrong_password,
                backgroundColor: const Color(0xFFFF0033),
                duration: const Duration(seconds: 5),
                colorText: Colors.white,
              );
              break;
            default:
              if(e.message != null) {
                Get.snackbar(
                  AppLocalizations.of(context)!.screen_login_title,
                  e.message!,
                  backgroundColor: const Color(0xFFFF0033),
                  duration: const Duration(seconds: 5),
                  colorText: Colors.white,
                );
              }
          }
        }
      });
    }
  }
}
