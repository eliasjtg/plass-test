import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';
import 'package:plass_test/controller/auth.dart';
import 'package:plass_test/input/date_picker.dart';
import 'package:plass_test/screen/partial/or.dart';

/// Register Screen
class RegisterScreen extends GetView<AuthenticationController> {

  final Map<String, dynamic> _parameters = <String, dynamic>{};

  final Map<String, dynamic> _profile = <String, dynamic>{};

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  /// RegisterScreen constructor
  RegisterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
    body: SafeArea(
      child: Flex(
        direction: Axis.vertical,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(10),
            child: Image.asset(
              'assets/logo.png',
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Form(
                  key: _formKey,
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.all(20),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            AppLocalizations.of(context)!.screen_sign_in_title,
                            style: Theme.of(context).textTheme.headline6,),
                          const Padding(
                            padding: EdgeInsets.symmetric(
                              vertical: 10,
                            ),
                          ),
                          TextFormField(
                            decoration: InputDecoration(
                              labelText: AppLocalizations.of(context)!.
                              input_email_label,
                              hintText: AppLocalizations.of(context)!.
                              input_email_hint,
                              prefixIcon: const Icon(
                                Icons.email_outlined,
                                size: 20,
                              ),
                            ),
                            keyboardType: TextInputType.emailAddress,
                            validator: MultiValidator(<FieldValidator<String?>>[
                              RequiredValidator(
                                errorText: AppLocalizations.of(context)!
                                    .validation_email_required,
                              ),
                              EmailValidator(
                                errorText: AppLocalizations.of(context)!
                                    .validation_email_invalid,
                              )
                            ]),
                            onSaved: (String? value) {
                              _parameters['email'] = value;
                            },
                          ),
                          const Padding(
                              padding: EdgeInsets.symmetric(vertical: 5),
                          ),
                          TextFormField(
                            decoration: InputDecoration(
                              labelText: AppLocalizations.of(context)!.
                                input_name_label,
                              hintText: AppLocalizations.of(context)!.
                                input_name_hint,
                              prefixIcon: const Icon(
                                Icons.person,
                                size: 20,
                              ),
                            ),
                            validator: RequiredValidator(
                              errorText: AppLocalizations.of(context)!
                                  .validation_name_required,
                            ),
                            onSaved: (String? value) {
                              _profile['name'] = value;
                            },
                          ),
                          const Padding(
                            padding: EdgeInsets.symmetric(vertical: 5),
                          ),
                          TextFormField(
                            decoration: InputDecoration(
                              labelText: AppLocalizations.of(context)!.
                                input_last_name_label,
                              hintText: AppLocalizations.of(context)!.
                                input_last_name_hint,
                              prefixIcon: const Icon(
                                Icons.person,
                                size: 20,
                              ),
                            ),
                            validator: RequiredValidator(
                              errorText: AppLocalizations.of(context)!
                                  .validation_last_name_required,
                            ),
                            onSaved: (String? value) {
                              _profile['last_name'] = value;
                            },
                          ),
                          const Padding(
                              padding: EdgeInsets.symmetric(vertical: 5),
                          ),
                          DatePickerField(
                            minValue: DateTime.now().subtract(
                                const Duration(days: 120 * 365),
                            ),
                            maxValue: DateTime.now(),
                            startValue: DateTime.now(),
                            onSaved: (DateTime? value) => _profile['birthdate']
                              = value?.millisecondsSinceEpoch,
                          ),
                          const Padding(
                              padding: EdgeInsets.symmetric(vertical: 5),
                          ),
                          TextFormField(
                            obscureText: true,
                            decoration: InputDecoration(
                              labelText: AppLocalizations.of(context)!.
                              input_password_label,
                              hintText: AppLocalizations.of(context)!.
                              input_password_hint,
                              prefixIcon: const Icon(
                                Icons.password,
                                size: 20,
                              ),
                            ),
                            onChanged: (String value) =>
                              _parameters['password'] = value,
                            validator: MultiValidator(<FieldValidator<String?>>[
                              RequiredValidator(
                                errorText: AppLocalizations.of(context)!
                                    .validation_password_required,
                              ),
                              MinLengthValidator(
                                8,
                                errorText: AppLocalizations.of(context)!
                                    .validation_password_min,
                              )
                            ]),
                            onSaved: (String? value) {
                              _parameters['password'] = value;
                            },
                          ),
                          const Padding(
                              padding: EdgeInsets.symmetric(vertical: 5),
                          ),
                          TextFormField(
                            obscureText: true,
                            decoration: InputDecoration(
                              labelText: AppLocalizations.of(context)!.
                                input_password_confirm_label,
                              hintText: AppLocalizations.of(context)!.
                                input_password_confirm_hint,
                              prefixIcon: const Icon(
                                Icons.password,
                                size: 20,
                              ),
                            ),
                            validator: (String? value) => MatchValidator(
                              errorText: AppLocalizations.of(context)!
                                  .validation_password_confirm_match,)
                                .validateMatch(value ?? '',
                              _parameters['password'] ?? '',),
                            onSaved: (String? value) {
                              _parameters['password_confirmation'] = value;
                            },
                          ),
                          const Padding(
                              padding: EdgeInsets.symmetric(vertical: 10),
                          ),
                          ElevatedButton(
                            onPressed: () => _signIn(context),
                            child: Text(
                              AppLocalizations.of(context)!.button_sign_in,
                            ),
                          ),
                          const Or(),
                          OutlinedButton(
                            onPressed: () => Get.offAndToNamed('/login'),
                            child: Text(
                              AppLocalizations.of(context)!.button_login,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    ),
  );

  void _signIn(BuildContext context) {

    if(_formKey.currentState?.validate() ?? false) {
      _formKey.currentState!.save();
      controller.register(
        _parameters['email'],
        _parameters['password'],
        _profile,
      ).catchError((Object e, StackTrace trace) {
        if(e is FirebaseAuthException) {
          switch(e.code) {
            case 'email-already-in-use':
              Get.snackbar(
                AppLocalizations.of(context)!.screen_sign_in_title,
                AppLocalizations.of(context)!.auth_email_already_in_use,
                backgroundColor: const Color(0xFFFF0033),
                duration: const Duration(seconds: 5),
                colorText: Colors.white,
              );
              break;
            case 'weak-password':
              Get.snackbar(
                AppLocalizations.of(context)!.screen_sign_in_title,
                AppLocalizations.of(context)!.auth_weak_password,
                backgroundColor: const Color(0xFFFF0033),
                duration: const Duration(seconds: 5),
                colorText: Colors.white,
              );
              break;
            default:
              if(e.message != null) {
                Get.snackbar(
                  AppLocalizations.of(context)!.screen_sign_in_title,
                  e.message!,
                  backgroundColor: const Color(0xFFFF0033),
                  duration: const Duration(seconds: 5),
                  colorText: Colors.white,
                );
              }
          }
        }
      });
    }
  }
}
