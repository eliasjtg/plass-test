import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';
import 'package:plass_test/controller/auth.dart';

/// Recover Password Screen
class RecoverPasswordScreen extends GetView<AuthenticationController> {

  final Map<String, dynamic> _parameters = <String, dynamic>{};

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  /// RecoverPasswordScreen constructor
  RecoverPasswordScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
    body: SafeArea(
      child: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(AppLocalizations.of(context)!.
                      screen_recover_password_title,
                      style: Theme.of(context).textTheme.headline6,),
                    const Padding(
                      padding: EdgeInsets.symmetric(vertical: 10),
                    ),
                    TextFormField(
                      decoration: InputDecoration(
                        labelText: AppLocalizations.of(context)!.
                          input_email_label,
                        hintText: AppLocalizations.of(context)!.
                          input_email_hint,
                        prefixIcon: const Icon(
                          Icons.email_outlined,
                          size: 20,
                        ),
                      ),
                      keyboardType: TextInputType.emailAddress,
                      validator: MultiValidator(<FieldValidator<String?>>[
                        RequiredValidator(
                          errorText: AppLocalizations.of(context)!
                              .validation_email_required,
                        ),
                        EmailValidator(
                          errorText: AppLocalizations.of(context)!
                              .validation_email_invalid,
                        )
                      ]),
                      onSaved: (String? value) {
                        _parameters['email'] = value;
                      },
                    ),
                    const Padding(
                      padding: EdgeInsets.symmetric(vertical: 5),
                    ),
                    ElevatedButton(
                      onPressed: () => _recover(context),
                      child: Text(
                        AppLocalizations.of(context)!.button_recover_password,
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.symmetric(vertical: 5),
                    ),
                    OutlinedButton(
                      onPressed: () => Get.offAndToNamed('/login'),
                      child: Text(
                        AppLocalizations.of(context)!.
                        button_login,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    ),
  );

  void _recover(BuildContext context) {
    if(_formKey.currentState?.validate() ?? false) {
      _formKey.currentState!.save();
      controller.recover(_parameters['email'])
        .then((_) {
          Get..snackbar(
            AppLocalizations.of(context)!.screen_recover_password_title,
            AppLocalizations.of(context)!.auth_recover_password_emailed,
            backgroundColor: Colors.green,
            duration: const Duration(seconds: 5),
            colorText: Colors.white,
          )..offAndToNamed('/login');
        }).catchError((Object e, StackTrace trace) {
        if(e is FirebaseAuthException) {
          if(e.message != null) {
            Get.snackbar(
              AppLocalizations.of(context)!.screen_recover_password_title,
              e.message!,
              backgroundColor: const Color(0xFFFF0033),
              duration: const Duration(seconds: 5),
              colorText: Colors.white,
            );
          }
        }
      });
    }
  }
}
