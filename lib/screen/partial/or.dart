import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

/// Or widget
class Or extends StatelessWidget {

  /// [vertical]
  final double vertical;

  /// Constructor Or
  const Or({this.vertical = 20, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Padding(
    padding: EdgeInsets.symmetric(vertical: vertical),
    child: Row(
      children: <Widget>[
        const Expanded(child: Divider(thickness: 1.2)),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 5),
          child: Text(AppLocalizations.of(context)!.or,
            style: Theme.of(context).textTheme.subtitle1,
          ),
        ),
        const Expanded(child: Divider(thickness: 1.2)),
      ],
    ),
  );
}
