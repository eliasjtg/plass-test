import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:plass_test/controller/auth.dart';
import 'package:plass_test/firebase_options.dart';
import 'package:plass_test/middleware/auth.dart';
import 'package:plass_test/middleware/guest.dart';
import 'package:plass_test/screen/home.dart';
import 'package:plass_test/screen/login.dart';
import 'package:plass_test/screen/recover_password.dart';
import 'package:plass_test/screen/register.dart';
import 'package:plass_test/screen/splash.dart';


/// Main
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  Get.lazyPut(AuthenticationController.new);
  runApp(const App());
}

/// App Widget
/// Root of application
class App extends StatelessWidget {

  /// App constructor
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => GetMaterialApp(
      title: 'Test PLASS Elias',
      debugShowCheckedModeBanner: false,
      localizationsDelegates: const <LocalizationsDelegate<dynamic>>[
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate
      ],
    theme: ThemeData(
      inputDecorationTheme: const InputDecorationTheme(
        labelStyle: TextStyle(fontSize: 13),
        contentPadding: EdgeInsets.symmetric(horizontal: 10),
        border: OutlineInputBorder(),
      ),
      cardTheme: CardTheme(
        elevation: 0,
        shadowColor: const Color(0xFFE8ECEF),
        shape: RoundedRectangleBorder(
          side: const BorderSide(color: Color(0xFFE8ECEF)),
          borderRadius: BorderRadius.circular(12),
        ),
      ),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
          minimumSize: MaterialStateProperty.all<Size>(
            const Size(double.infinity, 50),
          ),
          padding: MaterialStateProperty.all<EdgeInsets>(
            const EdgeInsets.symmetric(horizontal: 20),
          ),
        ),
      ),
      outlinedButtonTheme: OutlinedButtonThemeData(
        style: ButtonStyle(
          minimumSize: MaterialStateProperty.all<Size>(
            const Size(double.infinity, 50),
          ),
          padding: MaterialStateProperty.all<EdgeInsets>(
            const EdgeInsets.symmetric(horizontal: 20),
          ),
        ),
      ),
    ),
    supportedLocales: AppLocalizations.supportedLocales,
    initialRoute: '/splash',
    getPages: <GetPage<dynamic>>[
        GetPage<dynamic>(
            name: '/home',
            page: () => const HomeScreen(),
            middlewares: <GetMiddleware>[
              AuthMiddleware()
            ],
        ),
        GetPage<dynamic>(
            name: '/splash',
            title: 'Splash',
            page: () => const SplashScreen(),
            middlewares: <GetMiddleware>[
              GuestMiddleware()
            ],
        ),
        GetPage<dynamic>(
            name: '/login',
            title: 'Login',
            page: LoginScreen.new,
            middlewares: <GetMiddleware>[
              GuestMiddleware()
            ],
        ),
        GetPage<dynamic>(
            name: '/register',
            title: 'Register',
            page: RegisterScreen.new,
            middlewares: <GetMiddleware>[
              GuestMiddleware()
            ],
        ),
        GetPage<dynamic>(
            name: '/recover-password',
            title: 'Recover password',
            page: RecoverPasswordScreen.new,
            middlewares: <GetMiddleware>[
              GuestMiddleware()
            ],
        ),
      ],
    );
}
