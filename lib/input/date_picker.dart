import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:plass_test/input/callback.dart';

/// DatePickerField
class DatePickerField extends StatefulWidget {
  /// DatePickerField construct
  const DatePickerField({
    Key? key,
    this.enabled = true,
    this.onChanged,
    this.onSaved,
    this.initialValue,
    this.startValue,
    this.minValue,
    this.maxValue,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => DatePickerFieldState();

  /// [onChanged] callback
  final Function(DateTime)? onChanged;

  /// [onSaved] callback
  final FormFieldSetter<DateTime?>? onSaved;

  /// Field is [enabled]
  final bool enabled;

  /// Field [initialValue]
  final DateTime? initialValue;

  /// Field [startValue]
  final DateTime? startValue;

  /// Field [minValue]
  final DateTime? minValue;

  /// Field [maxValue]
  final DateTime? maxValue;
}

/// DatePickerField State
class DatePickerFieldState extends State<DatePickerField> {
  final GlobalKey<FormFieldState<String?>> _key =
  GlobalKey<FormFieldState<String?>>();

  final TextEditingController _controller = TextEditingController();

  DateTime? _currentValue;

  /// Current value
  DateTime? get currentValue => _currentValue;

  DateTime get _currentOrDefault =>
      currentValue ??
      widget.startValue ??
      widget.initialValue ??
      widget.minValue ??
      DateTime.now();

  void _setCurrent(DateTime? value, {bool listen = true}) {
    if (value != null) {
      _currentValue = value;
      _controller.text = '${value.year}-${value.month}-${value.day}';
      _key.currentState?.validate();
      if (listen) {
        widget.onChanged?.call(value);
      }
    }
  }

  /// Reset field
  void reset() => _controller.clear();

  @override
  void initState() {
    _setCurrent(widget.initialValue, listen: false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) => TextFormField(
    key: _key,
    readOnly: true,
    controller: _controller,
    onTap: () {
      FocusScope.of(context).requestFocus(FocusNode());
      if (widget.enabled) {
        showDatePicker(
          context: context,
          initialDate: _currentOrDefault,
          firstDate: widget.minValue ?? DateTime.now(),
          lastDate: widget.maxValue ??
              DateTime.now().add(const Duration(days: 30)),
        ).then(_setCurrent);
      }
    },
    style: const TextStyle(
      fontSize: 13,
      fontWeight: FontWeight.normal,
    ),
    decoration: InputDecoration(
      labelText: AppLocalizations.of(context)!.input_birthdate_label,
      hintText: AppLocalizations.of(context)!.input_birthdate_hint,
      prefixIcon: const Icon(
        Icons.today,
      ),
    ),
    onSaved: (String? time) {
      widget.onSaved?.call(currentValue);
    },
    validator: CallbackValidator<String?>(
      errorText: AppLocalizations.of(context)!.validation_birthdate_required,
      validator: (String? _) => currentValue != null,
    ),
  );
}
