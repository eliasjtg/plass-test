import 'package:form_field_validator/form_field_validator.dart';

/// Function signature for CallbackValidator
typedef CallbackValidatorFunction<T> = bool Function(T value);

/// Calls the given callback to validate an input.
class CallbackValidator<T> extends FieldValidator<T> {
  /// Callback validator construct
  CallbackValidator({
    required String errorText,
    required this.validator,
  }) : super(errorText);

  /// Callback [validator]
  final CallbackValidatorFunction<T> validator;

  @override
  bool isValid(T value) => validator(value);
}
