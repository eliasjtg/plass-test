import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:plass_test/controller/auth.dart';
import 'package:plass_test/state/auth.dart';

/// Guest middleware
class GuestMiddleware extends GetMiddleware {
  @override
  RouteSettings? redirect(String? route) {
    final AuthenticationController _authenticationController = Get.find();

    if (_authenticationController.state is Authenticated) {
      return const RouteSettings(name: '/home');
    }
    return null;
  }
}
