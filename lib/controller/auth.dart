import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'package:plass_test/model/user.dart' as model_user;
import 'package:plass_test/state/auth.dart';

/// Authentication controller
class AuthenticationController extends GetxController {
  final Rx<AuthenticationState> _authenticationStateStream =
      const AuthenticationState().obs;

  /// Current [state]
  AuthenticationState get state => _authenticationStateStream.value;

  late final StreamSubscription<User?> _userChangeSubscription;

  late final StreamSubscription<User?> _authStateChangesSubscription;

  DocumentReference<Map<String, dynamic>> _getUserReference(User user) =>
      FirebaseFirestore.instance
          .collection('users')
          .doc(user.uid);

  Future<model_user.User> _populateUser(User user) async
  => model_user.User.fromJson(
    user,
    (await _getUserReference(user).get()).data()!,
  );

  @override
  void onInit() {
    _getAuthenticatedUser();
    _userChangeSubscription = FirebaseAuth.instance
        .userChanges()
        .listen((User? user) async {
      if (user != null) {
        _authenticationStateStream.value = Authenticated(
          user: await _populateUser(user),
        );
      }
    });
    _authStateChangesSubscription = FirebaseAuth.instance
        .authStateChanges()
        .listen((User? user) async {
      if (user != null) {
        _authenticationStateStream.value = Authenticated(
          user: await _populateUser(user),
        );
        await Get.offAndToNamed('/home');
      } else {
        _authenticationStateStream.value = UnAuthenticated();
        if(Get.currentRoute != '/splash') {
          await Get.offAndToNamed('/login');
        }
      }
    });
    super.onInit();
  }

  @override
  void onClose() {
    _userChangeSubscription.cancel();
    _authStateChangesSubscription.cancel();
    super.onClose();
  }

  /// Sign in
  Future<UserCredential> signIn(String email, String password)
  => FirebaseAuth.instance.signInWithEmailAndPassword(
    email: email,
    password: password,
  ).then(
        (UserCredential value) {
      Get.offAndToNamed('/home');
      return value;
    },
  );

  /// Register
  Future<UserCredential> register(
      String email,
      String password,
      Map<String, dynamic> profile,
      )=> FirebaseAuth.instance.createUserWithEmailAndPassword(
    email: email,
    password: password,
  ).then(
        (UserCredential value) {
      _getUserReference(value.user!).set(profile);
      Get.offAndToNamed('/home');
      return value;
    },
  );

  /// Register
  Future<void> recover(
      String email,
    ) => FirebaseAuth.instance.sendPasswordResetEmail(
    email: email,
  );

  /// Sign out
  Future<void> signOut() => FirebaseAuth.instance.signOut();

  Future<void> _getAuthenticatedUser() async {
    _authenticationStateStream.value = AuthenticationLoading();

    final User? user = FirebaseAuth.instance.currentUser;

    if (user == null) {
      _authenticationStateStream.value = UnAuthenticated();
    } else {
      _authenticationStateStream.value = Authenticated(
          user: await _populateUser(user),
      );
    }
  }
}
