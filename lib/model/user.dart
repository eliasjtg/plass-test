import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart' as firebase_auth;
import 'package:intl/intl.dart';

/// User model
class User extends Equatable {
  /// User [auth]
  final firebase_auth.User auth;

  /// User [name]
  final String name;

  /// User [lastName]
  final String lastName;

  /// User [birthdate]
  final int birthdate;

  /// User constructor
  const User({
    required this.auth,
    required this.name,
    required this.lastName,
    required this.birthdate,
  });

  /// User full name
  String getFullName() => '$name $lastName';

  /// User full name
  String getStrBirthdate() => DateFormat('dd/MM/yyyy').format(
      DateTime.fromMillisecondsSinceEpoch(birthdate, isUtc: true),
  );

  /// Create new instance from Json
  factory User.fromJson(firebase_auth.User auth, Map<String, dynamic> json)
  => User(
    auth: auth,
    name: json['name']! as String,
    lastName: json['last_name']! as String,
    birthdate: json['birthdate']! as int,
  );

  /// Convert instance to Json representation
  Map<String, dynamic> toJson() => <String, dynamic>{
    'auth': <String, dynamic>{
      'displayName': auth.displayName,
      'email': auth.email,
      'emailVerified': auth.emailVerified,
      'isAnonymous': auth.isAnonymous,
      'metadata': auth.metadata,
      'phoneNumber': auth.phoneNumber,
      'photoURL': auth.photoURL,
      'providerData': auth.providerData,
      'refreshToken': auth.refreshToken,
      'tenantId': auth.tenantId,
      'uid': auth.uid,
    },
    'name': name,
    'last_name': lastName,
    'birthdate': birthdate,
  };

  @override
  List<Object?> get props => <Object?>[
    auth,
    name,
    lastName,
    birthdate,
  ];

  @override
  String toString() => toJson().toString();
}
